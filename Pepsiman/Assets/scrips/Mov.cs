using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mov : MonoBehaviour
{
    public Rigidbody rb;
    public Animator anim;
    [Range(0, 10)]
    public float Mspeed, pegar = 2f, Salto, Rtspeed;
    public bool flooryes, floor, ataque;
    public int Pepsis,pepso;
    public AudioClip saltito;
    public AudioSource source;
    public checkpoint check;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Pepsis = 0;
        pepso = 0;
    }
    private void Update()
    {

        mov();
        atack();
        jump();
        if (Pepsis==8)
        {
            SceneManager.LoadScene("2");
        }
        if (pepso==8)
        {
            SceneManager.LoadScene("win");
        }
    }
    public void mov()
    {
        Vector3 MovDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("speed", MovDirection.z);
        transform.Translate((MovDirection * Mspeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * Rtspeed, 0));
    }
    public void jump()
    {
        Vector3 vr3 = transform.TransformDirection(Vector3.down);
        if (Physics.Raycast(transform.position, vr3, 3f))
        {
            flooryes = true;
        }
        else
        {
            flooryes = false;
        }
        floor = Input.GetButton("Jump");
        if (floor && flooryes)
        {
           
            rb.AddForce(new Vector3(0, Salto, 0), ForceMode.Impulse);
            anim.SetBool("salto", true);
            if (Physics.Raycast(transform.position, vr3, 0.3f))
            {
                //AudioSource.PlayClipAtPoint(saltito, gameObject.transform.position);
                source.PlayOneShot(saltito);
            }
        }
        if (!floor && flooryes)
        {
            anim.SetBool("salto", false);
        }
    }
   public void atack()
    {
        ataque = Input.GetButton("Fire1");
        if (ataque)
        {
            rb.AddForce(new Vector3(0, pegar, 0), ForceMode.Force);
            anim.SetBool("atacar", true);
        }
        if (!ataque)
        {
            anim.SetBool("atacar", false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pepsi")
        {
            other.GetComponentInParent<coleccionables>().pepsi();
            Pepsis++;
        }
        if (other.tag == "pepso")
        {
            other.GetComponentInParent<coleccionables>().pepso();
            pepso++;
        }
        if (other.transform.tag=="che")
        {
            Debug.Log("CHE");
            check = other.GetComponent<checkpoint>();
            check.gameObject.SetActive(false);
        }
        if (other.transform.tag=="Piso")
        {
            transform.position = check.spoint;
        }
    }
   



}
